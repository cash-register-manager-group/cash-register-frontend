import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';

import {ActivatedRoute} from "@angular/router";
import {DeviceService} from "../../services/device.service";
import {EventService} from "../../services/event.service";
import {BehaviorSubject, of} from "rxjs";
import {catchError, finalize} from "rxjs/operators";

@Component({
  selector: 'app-device-detail',
  templateUrl: './device-detail.component.html',
  styleUrls: ['./device-detail.component.css']
})
export class DeviceDetailComponent implements OnInit {

  TAG = 'DeviceDetailComponent';
  private device_id: string | null;
  protected loadingSubject = new BehaviorSubject<boolean>(false);
  public loading$ = this.loadingSubject.asObservable();


  // @ts-ignore
  public DeviceDetailEntity: DeviceDetailEntity;

  constructor(private route: ActivatedRoute,
              private deviceService: DeviceService,
              private eventService: EventService,
              public dialog: MatDialog) {
    this.device_id = null;
  }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.device_id = id;
    console.log(this.TAG, '.ngOnInit: id=', id);
    if (id != null) {
      this.load(id);
    } else {
      console.warn(this.TAG, '.ngOnInit: id is null !');
    }
  }


  public load(id: string): void {
    console.log(this.TAG, '.load: ...');
    this.deviceService.detail(id)
      .pipe(
      )
      .subscribe(restRequest => {
          console.log(this.TAG, '.load: res = ', restRequest);
          this.DeviceDetailEntity = restRequest;
          console.log(this.TAG, '.load: entry = ', this.DeviceDetailEntity);
        }
      );
  }


  openZDialog() {
    console.log(this.TAG, '.openZDialog: ...');
    const dialogRef = this.dialog.open(DeviceDetailDialog, {
      width: '300px',
      data: {title: 'Закрити зміну', description: 'Надруковати  Z звіт та обнулити дані в касовому апараті ?'}
    });
    dialogRef.afterClosed().subscribe(result => {
      console.info(this.TAG, '.openZDialog: The dialog was closed, result = ' + result);
    });
  }

  openXDialog() {
    console.log(this.TAG, '.openXDialog: ...');
    const dialogRef = this.dialog.open(DeviceDetailDialog, {
      width: '300px',
      data: {title: 'X звіт', description: 'Надруковати Х звіт ?'}
    });
    dialogRef.afterClosed().subscribe(result => {
      console.info(this.TAG, '.openXDialog: The dialog was closed, result = ' + result);
      if (result === true) {
        if (this.device_id !== null) {
          this.loadingSubject.next(true);
          this.eventService.reportX(this.device_id)
            .pipe(
              catchError(error => {
                console.warn(this.TAG, '.openXDialog: server returned error: ' + error);
                return of([]);
              }),
              finalize(() => this.loadingSubject.next(false))
            )
            .subscribe(restRequest => {
                console.log(this.TAG, '.load: res = ', restRequest);
                if ('error_code' in restRequest) {
                  console.log(this.TAG, '.load: error = ', restRequest['error_code']);
                } else {
                  console.warn(this.TAG, '.load: server returned incorrect data');
                }
              }
            );
        } else {
          console.warn(this.TAG, '.openXDialog: device_id is NULL, the event was ignored');
        }
      }
    });
  }


  openWarnDialog(title: String, txt: string) {
    console.log(this.TAG, '.openWarnDialog: ...');
    const dialogRef = this.dialog.open(DeviceDetailDialogWarn, {
      width: '300px',
      data: {title: title, description: txt}
    });

  }


}


export interface DialogData {
  title: string;
  description: string;
}

@Component({
  selector: 'device-detail-dialog',
  templateUrl: 'device-detail-dialog.component.html',
})
export class DeviceDetailDialog {

  constructor(
    public dialogRef: MatDialogRef<DeviceDetailDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onYesClick(): void {
    this.dialogRef.close();
  }
}


@Component({
  selector: 'device-detail-dialog',
  templateUrl: 'device-detail-warn-dialog.component.html',
})
export class DeviceDetailDialogWarn {

  constructor(
    public dialogRef: MatDialogRef<DeviceDetailDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
  }

}
