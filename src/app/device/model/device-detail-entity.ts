import {LocateEntity} from "./locate-entity";
import {TypeEntity} from "./type-entity";

export class DeviceDetailEntity {

  // @ts-ignore
  private _id: number;
  // @ts-ignore
  private _name: string;
  // @ts-ignore
  private _locate_id: LocateEntity;
  // @ts-ignore
  private _type_id: TypeEntity;
  // @ts-ignore
  private _ip: string;
  // @ts-ignore
  private _description: string;


  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get locate_id(): LocateEntity {
    return this._locate_id;
  }

  set locate_id(value: LocateEntity) {
    this._locate_id = value;
  }

  get type_id(): TypeEntity {
    return this._type_id;
  }

  set type_id(value: TypeEntity) {
    this._type_id = value;
  }

  get ip(): string {
    return this._ip;
  }

  set ip(value: string) {
    this._ip = value;
  }

  get description(): string {
    return this._description;
  }

  set description(value: string) {
    this._description = value;
  }
}
