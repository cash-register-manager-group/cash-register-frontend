export class EventEntity {

  // @ts-ignore
  private _id: number;
  // @ts-ignore
  private _device_id: number;
  // @ts-ignore
  private _date_create: string;
  // @ts-ignore
  private _error_code: string;
  // @ts-ignore
  private _error_msg: string;

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get device_id(): number {
    return this._device_id;
  }

  set device_id(value: number) {
    this._device_id = value;
  }

  get date_create(): string {
    return this._date_create;
  }

  set date_create(value: string) {
    this._date_create = value;
  }

  get error_code(): string {
    return this._error_code;
  }

  set error_code(value: string) {
    this._error_code = value;
  }

  get error_msg(): string {
    return this._error_msg;
  }

  set error_msg(value: string) {
    this._error_msg = value;
  }
}
