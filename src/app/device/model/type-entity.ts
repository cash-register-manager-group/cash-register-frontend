export class TypeEntity {

  // @ts-ignore
  private _id: number;
  // @ts-ignore
  private _name: string;
  // @ts-ignore
  private _manufacturer: number;
  // @ts-ignore
  private _model: number;
  // @ts-ignore
  private _image: string;


  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get manufacturer(): number {
    return this._manufacturer;
  }

  set manufacturer(value: number) {
    this._manufacturer = value;
  }

  get model(): number {
    return this._model;
  }

  set model(value: number) {
    this._model = value;
  }


  get image(): string {
    return this._image;
  }

  set image(value: string) {
    this._image = value;
  }
}
