import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DeviceRoutingModule} from './device-routing.module';
import {
  DeviceDetailComponent,
  DeviceDetailDialog,
  DeviceDetailDialogWarn
} from './device-detail/device-detail.component';
import {DeviceListComponent} from './device-list/device-list.component';
import {MatSliderModule} from "@angular/material/slider";
import {DeviceMainComponent} from './device-main/device-main.component';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatCardModule} from '@angular/material/card';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatExpansionModule} from '@angular/material/expansion';
import {LayoutModule} from '@angular/cdk/layout';
import {MatDialogModule} from "@angular/material/dialog";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {MatProgressBarModule} from "@angular/material/progress-bar";


@NgModule({
  declarations: [
    DeviceDetailComponent,
    DeviceListComponent,
    DeviceMainComponent,
    DeviceDetailDialog,
    DeviceDetailDialogWarn,
  ],
  imports: [
    CommonModule,
    DeviceRoutingModule,
    MatSliderModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    MatExpansionModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    LayoutModule
  ]
})
export class DeviceModule {
}
