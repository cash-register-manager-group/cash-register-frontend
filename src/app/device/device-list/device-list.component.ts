import {Component, OnInit} from '@angular/core';
import {DeviceService} from "../../services/device.service";
import {DeviceEntity} from "../model/device-entity";

@Component({
  selector: 'app-device-list',
  templateUrl: './device-list.component.html',
  styleUrls: ['./device-list.component.css']
})
export class DeviceListComponent implements OnInit {

  constructor(private deviceService: DeviceService) {
  }

  // @ts-ignore
  cards:[DeviceEntity];


  ngOnInit(): void {
    this.load();
  }

  public load(): void {
    console.log('DeviceListComponent.load: ...');
    this.deviceService.list()
      .pipe(
      )
      .subscribe(restRequest => {
          console.log('DeviceListComponent.load: res = ', restRequest);
          this.cards = restRequest;
          restRequest.forEach(function (entry) {
            console.log('\'DeviceListComponent.load: entry = ', entry);
          });

        }
      );
  }


}
