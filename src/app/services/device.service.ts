import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {DeviceEntity} from "../device/model/device-entity";
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from "../../environments/environment";
import {map} from "rxjs/operators";
import {DeviceDetailEntity} from "../device/model/device-detail-entity";


@Injectable({
  providedIn: 'root'
})
export class DeviceService {

  private readonly PATCH = environment.PACH_DEVICE;
  private readonly url: string;


  constructor(private http: HttpClient) {
    this.url = environment.API_BASE_URL + this.PATCH;
  }


  public list(): Observable<[DeviceEntity]> {
    console.log('DeviceService.list: ...');
    return this.http.get<[DeviceEntity]>(this.url, {
      params: new HttpParams()
        .set('format', 'json')
    }).pipe(map(res => res));
  }

  public detail(id: string): Observable<DeviceDetailEntity> {
    console.log('DeviceService.detail: ...');
    const pach = this.url + id + '/';
    return this.http.get<DeviceDetailEntity>(pach, {
      params: new HttpParams()
        .set('format', 'json')
    }).pipe(map(res => res));
  }


}


