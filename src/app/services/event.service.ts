import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {DeviceEntity} from "../device/model/device-entity";
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from "../../environments/environment";
import {map} from "rxjs/operators";
import {DeviceDetailEntity} from "../device/model/device-detail-entity";
import {EventEntity} from "../device/model/event-entity";
import {ReportXEntity} from "../device/model/report-x-entity";


@Injectable({
  providedIn: 'root'
})
export class EventService {
  TAG = 'EventService';
  private readonly PATCH = environment.PACH_EVENT;

  private readonly url: string;


  constructor(private http: HttpClient) {
    this.url = environment.API_BASE_URL + this.PATCH;
  }


  public reportX(device_id: string): Observable<ReportXEntity> {
    console.log(this.TAG, '.reportX: device_id = ', device_id);
    const url: string = environment.API_BASE_URL + environment.PACH_REPORT_X
    return this.http.post<ReportXEntity>(url, {
      format: 'json',
      device_id: device_id,
    });
  }

}


