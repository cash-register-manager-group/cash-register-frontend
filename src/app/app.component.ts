import {Component} from '@angular/core';
import {Breakpoints} from "@angular/cdk/layout";
import {map} from "rxjs/operators";
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'cash-registers-frontend';
  breadcrumbList: Array<any> = [];

  constructor(private _router: Router) {
  }


}
