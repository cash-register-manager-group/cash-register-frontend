// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  PACH_DEVICE: '/device/',
  PACH_EVENT: '/event/',
  PACH_REPORT_X: '/event/xreport/create/',
  PACH_REPORT_Y: '/event/yreport/create/',

  API_BASE_URL: 'http://127.0.0.1:8000',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
